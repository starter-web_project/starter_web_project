import os
from flask import Flask
# from flask_login import LoginManager
from dotenv import load_dotenv
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from flask_cors import CORS

from flask_caching import Cache 

from nomina_app.config import config
# from config import config


db = SQLAlchemy()
csrf = CSRFProtect()
cors  = CORS()
cache = Cache()

# login_manager = LoginManager()
#login_manager.login_view = '.blueprints.auth.login'

def create_app(config_name):
    app = Flask(__name__)

    flaskenv_path = os.path.join(os.path.dirname(__file__), '.flaskenv')
    if os.path.exists(flaskenv_path):
        load_dotenv(flaskenv_path)

    app.config.from_object(config_name)
    config_name.init_app(app)

    db.init_app(app)
    csrf.init_app(app)
    cors.init_app(app)
    cache.init_app(app)

    # login_manager.init_app(app)

    # Blueprints registration
    from .blueprints.auth.gen_csrf_tk import gen_tk as gen_tk_bp
    app.register_blueprint(gen_tk_bp)
    
    from .blueprints.auth.check_csrf_tk import check_csrf_tk as chk_tk_bp
    app.register_blueprint(chk_tk_bp)

    # from .blueprints.auth import auth as auth_bp
    # app.register_blueprint(auth_bp, url_prefix='/auth')    

    if __name__ == "__main__":
        app.run()

    return app