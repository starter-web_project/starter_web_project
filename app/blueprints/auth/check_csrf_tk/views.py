from flask import request, jsonify, make_response, render_template, \
    g, current_app

from flask_wtf.csrf import validate_csrf, CSRFError
import logging

from . import check_csrf_tk

@check_csrf_tk.before_app_request
def verify_token():
    if request.headers.get("X-Xsrf-Token"):
        print("===== Desde verify_token rutasBP =====")
        req_tk = request.headers.get("X-Xsrf-Token")
        print(req_tk)
        # req_tk = request.headers['X_XSRF_TOKEN']
        loc_tk = current_app.config["TOKEN_VALIDO"]
        print(loc_tk)
        if req_tk and (req_tk != loc_tk):
            validate_csrf(req_tk)
            logging.warning("Crea un nuevo token")
            return make_response({"msg": "Integridad de Token Fallida"})
        else:
            logging.info("Prueba de Integridad Exitosa")
    
@check_csrf_tk.route('/index', methods=['POST'])
def prueba():
    print("Desde Prueba Index Endpoint")
    request.data
    print(request.data)

    return make_response({"msg": "Tu mensaje ha sido recibido"})


