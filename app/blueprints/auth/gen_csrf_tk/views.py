from flask import render_template, session, \
    request, current_app, make_response, jsonify, g

from flask_caching.backends import SimpleCache
from flask_wtf.csrf import generate_csrf, validate_csrf

from nomina_app.app.blueprints.auth.gen_csrf_tk import gen_tk


@gen_tk.after_request
def inject_csrf_token(response):

    xsrf_token = generate_csrf()

    from nomina_app.main import app

    cookie_name = app.config["SECURITY_CSRF_COOKIE"]["key"]
    app.config["TOKEN_VALIDO"] = xsrf_token

    response.headers['X-XSRF-Token'] = xsrf_token
    response.set_cookie(cookie_name, xsrf_token, samesite="Strict")

    return response


@gen_tk.route('/', methods=['GET'])
def index():    
    return jsonify({"msg": "Tu token ha sido generado de manera exitosa"})
    
