from app import db, email
from app.models import user
from flask import render_template, redirect, request, url_for, flash
from flask_login import login_user, logout_user, login_required, current_user
from . import auth
from .forms import LoginForm, RegistrationForm, ChangePasswordForm, PasswordResetForm, PasswordResetRequestForm


@auth.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.ping()
        if not current_user.confirmed \
                and request.endpoint \
                and request.blueprint != 'auth' \
                and request.endpoint != 'static':
            return redirect(url_for('auth.unconfirmed'))


@auth.route('/confirm/<token>')
@login_required
def confirm(token):
    if current_user.confirmed:
        return redirect(url_for('main.index'))
    if current_user.confirm(token):
        print("confirm(token):", current_user)
        db.session.commit()
        flash('You have confirmed your account. Thanks!')
    else:
        flash('The confirmation link is invalid or has expired.')
    return redirect(url_for('main.index'))


@auth.route('/confirm')
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    email.send_email(current_user.email, 'Confirm Your Account',
               'auth/email/confirm', user=current_user, token=token)
    flash('A new confirmation email has been sent to you by email.')
    return redirect(url_for('main.index'))


@auth.route('/unconfirmed')
def unconfirmed():
    if current_user.is_anonymous or current_user.confirmed:
        print(current_user)
        return redirect(url_for('main.index'))
    return render_template('auth/email/unconfirmed.html')


@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        welcome_user = user.User.query.filter_by(email=form.email.data.lower()).first()
        if welcome_user is not None and welcome_user.verify_password(form.password.data):
            login_user(welcome_user, form.remember_me.data)
            next = request.args.get('next')
            if next is None or not next.startswith('/'):
                next = url_for('main.index')
                return redirect(next)
        flash('Invalid email or password.')
    return render_template('auth/login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have been logged out.')
    return redirect(url_for('main.index'))


@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        new_user = user.User(email=form.email.data.lower(),
                    username=form.username.data,
                    password=form.password.data)
        db.session.add(new_user)
        db.session.commit()
        token = new_user.generate_confirmation_token()
        email.send_email(new_user.email, 'Confirm Your Account',
        'auth/email/confirm', user=new_user, token=token)
        flash('A confirmation email has been sent to you by email.')
        login_user(new_user)
        return redirect(url_for('auth.login'))
    return render_template('auth/sign_up.html', form=form)


@auth.route('/change-password', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.password.data
            db.session.add(current_user)
            db.session.commit()
            flash('Your password has been updated.')
            return redirect(url_for('main.index'))
        else:
            flash('Invalid password.')
    return render_template("auth/password/change_password.html", form=form)


@auth.route('/reset', methods=['GET', 'POST'])
def password_reset_request():
    if not current_user.is_anonymous:
        return redirect(url_for('main.index'))
    else:
        form = PasswordResetRequestForm()
        if form.validate_on_submit():
            user_reset_pw = user.User.query.filter_by(email=form.email.data.lower()).first()
            if user_reset_pw:
                token = user_reset_pw.generate_reset_token()
                email.send_email(user_reset_pw.email, 'Reset Your Password',
                        'auth/email/reset_password',
                        user=user_reset_pw, token=token)
                flash('An email with instructions to reset your password has been '
                'sent to you.')
            flash('Are you sure you have registered this email ? ', "warning")
            return redirect(url_for('auth.login'))
    return render_template('auth/password/reset_password.html', form=form)


@auth.route('/reset/<token>', methods=['GET', 'POST'])
def password_reset(token):
    if not current_user.is_anonymous:
        return redirect(url_for('main.index'))
    else:
        form = PasswordResetForm()
        if form.validate_on_submit():
            if user.User.reset_password(token, form.password.data):
                db.session.commit()
                flash('Your password has been updated.')
                return redirect(url_for('auth.login'))
            else:
                return redirect(url_for('main.index'))
    return render_template('auth/password/reset_password.html', form=form)