import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config():
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'MSK-MySecretKey'
    WTF_CSRF_SECRET_KEY = os.environ.get('WTF_CSRF_SECRET_KEY') or 'MSK-SecureToken'

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Flask caching Config
    CACHE_TYPE = "simple" # Flask-Caching related configs
    CACHE_DEFAULT_TIMEOUT = 300
    TOKEN_VALIDO = ''


    # Send Cookie with csrf-token. This is the default for Axios and Angular.
    SECURITY_CSRF_COOKIE = {"key": "XSRF-TOKEN"}
    SECURITY_CSRF_HEADER = "X-XSRF-TOKEN"
    WTF_CSRF_CHECK_DEFAULT = False
    WTF_CSRF_TIME_LIMIT = None

    # Redirect to Angular by default
    SECURITY_REDIRECT_BEHAVIOR = "spa"
    SECURITY_REDIRECT_HOST = "localhost:4200"

    # Mail config envs 
#    FLASKY_ADMIN= os.environ.get('FLASKY_ADMIN')

    @staticmethod
    def init_app(app):
        pass


class TestingConf(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'TEST_DATABASE_URL') or 'sqlite:///'


class DevelopmentConf(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DEV_DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')


class ProductionConf(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'data.sqlite')


config = {
    'development': DevelopmentConf,
    'production': ProductionConf,
    'Testing': TestingConf,

    'default': DevelopmentConf
}